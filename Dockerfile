FROM node:latest

# Membuat direktori kerja di dalam container
WORKDIR /app

# Menyalin package.json dan package-lock.json untuk instalasi dependensi
COPY package*.json ./

# Menginstal dependensi
RUN npm install

# Menyalin kode sumber aplikasi
COPY . .

# Membangun aplikasi Next.js
RUN npm run build

# Menjalankan aplikasi
CMD ["npm", "start"]
