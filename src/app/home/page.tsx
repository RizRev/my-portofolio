import Image from "next/image"

export default async function Page() {
    return (
        <div>
            <div style={{
                height: 50,
                width: 1365,
                marginInline: "auto",
                borderInlineWidth: 1,
                borderColor:"#2A323E",
            }}>
                <div style={{
                    height: 50,
                    width: 455,
                    marginInline: "auto",
                    borderInlineWidth: 1,
                    borderColor:"#2A323E",
                }}>
                </div>
            </div>
            <div style={{
                height: 210,
                borderTopWidth:1,
                borderBottomWidth:1,
                borderColor:"#2A323E",
            }}>
                <div style={{
                    // padding: "auto",
                    height: "100%",
                    display: 'flex',
                    flexDirection: 'row',
                    width: 1365,
                    borderInlineWidth: 1,
                    borderColor:"#2A323E",
                    marginInline: "auto",
                    alignItems: "center",
                }}>
                    <p style={{
                        fontSize:120,
                        fontWeight:"bold",
                        marginLeft: 70
                    }}>Revanda .</p>
                    <div style={{
                        paddingTop: 3,
                        width: 100,
                        height: 35,
                        marginTop: -80,
                        backgroundColor:"orange",
                        borderRadius: 25
                    }}>
                        <p style={{
                            textAlign: "center"
                            // marginTop: -80
                            // alignSelf: 'flex-start'
                        }}>Hallo</p>
                    </div>
                </div>
            </div>
            {/* Row 2 */}
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'center',
                height: 515,
                borderBottomWidth: 1,
                borderColor:"#2A323E"
            }}>
                <div style={{
                    borderInlineWidth: 1,
                    borderColor:"#2A323E",
                    width: 455,
                    padding: 35
                }}>
                    <p style={{
                        fontSize: 46,
                        fontWeight: "bold",
                        marginBottom: 15
                    }}>About me</p>
                    <p style={{
                        fontSize: 17,
                        color: "#77818F",
                        marginBottom: 15
                    }}>I&apos;m from Indonesia and working as Developer. Build some Web and Apps to make your life so Easy</p>
                    <p style={{
                        fontSize: 17,
                        color: "#77818F",
                        marginBottom: 15
                    }}>likes to explore the latest Technology to achieve Perfect and Efficient results</p>
                </div>
                <div style={{
                    width: 455,
                }}>
                    <Image
                    style={{
                        margin: "auto",
                        marginTop: "35px", 
                        borderRadius: "20px", 
                        backgroundColor: "blue",
                        objectFit: "cover",
                        height:410
                    }}
                    src="/me2.png"
                    alt="me"
                    width={410}
                    height={400}
                    priority
                    />
                </div>
                <div style={{
                    padding: "35px",
                    borderInlineWidth: 1,
                    borderColor:"#2A323E",
                    width: 455
                }}>
                    <p 
                    style={{
                        fontSize: "20px",
                        fontWeight: "bold",
                        color: "orangered",
                        marginBottom: 15
                    }}
                    >EXPERIMENT</p>
                    <div>
                        <div
                        style={{
                            borderRadius: 20,
                            borderWidth: 1,
                            borderColor:"#2A323E",
                            height: 105,
                            marginBottom: 22,
                        }}
                        >
                            <p
                            style={{
                                fontSize: 29,
                                fontWeight: "800",
                                marginTop: 13,
                                marginLeft: 23
                            }}
                            >Recipe</p>
                            <p
                            style={{
                                color: "#77818F",
                                marginLeft: 23
                            }}
                            >Sharing Delicious Food</p>
                        </div>
                        <div
                        style={{
                            borderRadius: 20,
                            borderWidth: 1,
                            borderColor:"#2A323E",
                            height: 105,
                            marginBottom: 22
                        }}
                        >
                            <p
                            style={{
                                fontSize: 29,
                                fontWeight: "800",
                                marginTop: 13,
                                marginLeft: 23
                            }}
                            >Ticket</p>
                            <p
                            style={{
                                color: "#77818F",
                                marginLeft: 23
                            }}
                            >Traveling all the time</p>
                        </div>
                        <div
                        style={{
                            borderRadius: 20,
                            borderWidth: 1,
                            borderColor:"#2A323E",
                            height: 105,
                            marginBottom: 22
                        }}
                        >
                            <p style={{
                                fontSize: 29,
                                fontWeight: "800",
                                marginTop: 13,
                                marginLeft: 23
                            }}
                            >Book Galery</p>
                            <p style={{
                                color: "#77818F",
                                marginLeft: 23
                            }}>Window of world</p>
                        </div>
                    </div>
                </div>
            </div>
            {/* Row 3 */}
            <div style={{
                height: 350,
                width: 1365,
                marginInline: "auto",
                borderInlineWidth: 1,
                borderBottomWidth: 1,
                borderColor:"#2A323E",
            }}>
                <div style={{
                    height: 120,
                    fontWeight: 500,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",   
                    alignSelf: "center"
                }}>
                    <p style={{
                        fontSize: 24,
                    }}>Companies I&apos;ve worked with</p>
                </div>
                <div style={{
                    display: "flex",
                    flexDirection: 'row',
                    justifyContent: "space-evenly"
                }}>
                    <div style={{
                        width: 400,
                        display: "flex",
                        flexDirection: 'column',
                        justifyContent: "center",
                        alignItems: "center",
                    }}>
                        <Image
                        style={{
                        }}
                        alt="pegadaian-logo"
                        src="/pagadaian-monochrome.png"
                        height={100}
                        width={300}
                        priority
                        />

                        <p style={{
                            textAlign: "center",
                            color: "#77818F",
                        }}>PT Pegadaian, Indonesia&apos;s largest pawnshop, plays a crucial role in community empowerment by providing accessible financial services and loans.</p>
                    </div>
                    <div style={{
                        width: 400,
                        display: "flex",
                        flexDirection: 'column',
                        justifyContent: "center",
                        alignItems: "center",
                    }}>
                        <Image
                        style={{
                        }}
                        alt="pegadaian-logo"
                        src="/prodigium-monochrome.png"
                        height={100}
                        width={200}
                        priority
                        />
                        <p style={{
                            textAlign: 'center',
                            color: "#77818F",
                        }}>
                        Prodigium, a Jakarta startup, excels in room cleaning services, making a significant impact on cleanliness for a wide consumer base.</p>
                    </div>
                </div>
            </div>
            {/* Row 4 */}
            <div style={{
                height: 400,
                width: 1365,
                marginInline: "auto",
                borderInlineWidth: 1,
                borderBottomWidth: 1,
                borderColor:"#2A323E",
            }}>
                <div style={{
                    height: 120,
                    fontWeight: 500,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",   
                    alignSelf: "center"
                }}>
                    <p style={{
                        fontSize: 24,
                        opacity: 0.8
                    }}>My Stack Development</p>                    
                </div>
                <div  style={{
                    display: "flex",
                    justifyContent: "space-evenly"
                }}>
                    <div style={{
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "center",
                        alignSelf: "center",
                        alignItems: "center"
                    }}>
                        <div style={{
                            width: 130,
                            display: "flex",
                            flexDirection: "row",
                            flexWrap: "wrap",
                            justifyContent: "space-between",
                            rowGap: 15
                        }}>
                            <Image
                            src="/Next - Mono.png"
                            alt="Next"
                            width={54}
                            height={54}
                            priority
                            />
                            <Image
                            src="/React - Mono.png"
                            alt="React"
                            width={54}
                            height={54}
                            priority
                            />
                            <Image
                            src="/Vue - Mono.png"
                            alt="Vue"
                            width={54}
                            height={54}
                            priority
                            />
                        </div>
                        <p style={{
                            marginTop: 25
                        }}>Front-End Framework</p>
                    </div>
                    <div style={{
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "center",
                        alignSelf: "center",
                        alignItems: "center"
                    }}>
                        <div style={{
                            width: 130,
                            display: "flex",
                            flexDirection: "row",
                            flexWrap: "wrap",
                            justifyContent: "center"
                        }}>
                            <Image
                            src="/Pg - Mono.png"
                            alt="Pg"
                            width={54}
                            height={54}
                            priority
                            />
                        </div>
                        <p style={{
                            marginTop: 25
                        }}>Database</p>
                    </div>
                    <div style={{
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "center",
                        alignSelf: "center",
                        alignItems: "center"
                    }}>
                        <div style={{
                            width: 130,
                            display: "flex",
                            flexDirection: "row",
                            flexWrap: "wrap",
                            justifyContent: "space-between",
                            rowGap: 15
                        }}>
                            <Image
                            src="/Node - Mono.png"
                            alt="Node"
                            width={54}
                            height={54}
                            priority
                            />
                            <Image
                            src="/CSS - Mono.png"
                            alt="CSS"
                            width={54}
                            height={54}
                            priority
                            />
                            <Image
                            src="/HTML - Mono.png"
                            alt="HTML"
                            width={54}
                            height={54}
                            priority
                            />
                            <Image
                            src="/JS - Mono.png"
                            alt="JS"
                            width={54}
                            height={54}
                            priority
                            />
                            <Image
                            src="/Ts - Mono.png"
                            alt="Ts"
                            width={54}
                            height={54}
                            priority
                            />
                        </div>
                        <p style={{
                            marginTop: 25
                        }}>Basic</p>
                    </div>
                    <div style={{
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "center",
                        alignSelf: "center",
                        alignItems: "center"
                    }}>
                        <div style={{
                            width: 130,
                            display: "flex",
                            flexDirection: "row",
                            flexWrap: "wrap",
                            justifyContent: "center"
                        }}>
                            <Image
                            style={{
                                backgroundColor: "#1a1a1a",
                                borderRadius: 15
                            }}
                            src="/Express - Mono.png"
                            alt="Express"
                            width={54}
                            height={54}
                            priority
                            />
                        </div>
                        <p style={{
                            marginTop: 25
                        }}>Backend</p>
                    </div>
                    <div style={{
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "center",
                        alignSelf: "center",
                        alignItems: "center"
                    }}>
                        <div style={{
                            width: 130,
                            display: "flex",
                            flexDirection: "row",
                            flexWrap: "wrap",
                            justifyContent: "center"
                        }}>
                            <Image
                            src="/Docker - Mono.png"
                            alt="Docker"
                            width={54}
                            height={54}
                            priority
                            />
                        </div>
                        <p style={{
                            marginTop: 25
                        }}>Development and Optimization</p>
                    </div>
                </div>
            </div>
            {/* Row 4 */}
            <div style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",   
                alignSelf: "center",
                height: 220,
                width: 1365,
                marginInline: "auto",
                marginBottom: 70,
                borderInlineWidth: 1,
                borderBottomWidth: 1,
                borderColor:"#2A323E",
            }}>
                <p>I&apos;m looking for freelance, enjoy to share your project with me</p>
                <p>Call me</p>
                <p>2024 </p>
            </div>
        </div>
    )
}
